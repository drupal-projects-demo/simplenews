# simplenews

[Simplenews](https://www.drupal.org/project/simplenews) publishes and sends newsletters to lists of subscribers.

# Install
* Follow "Installation instructions" (README.txt)
* At the end, Newsletters may not be in html.
